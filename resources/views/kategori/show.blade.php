@extends('layout.master')
@section('judul')
    Detail Kategori
@endsection

@section('content')

<h1>{{$kategori->name}}</h1>
<p>{!! $kategori->description !!}</p>

<div class="row">
    @forelse ($kategori->berita as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" >
            <div class="card-body">
              <h5>{{$item->title}}</h5>
              <p class="card-text">{{Str::limit($item->content, 50)}}</p>
             <form action="/berita/{{$item->id}}" method="POST">
                @method('DELETE')
                @csrf
                <a href="/berita/{{$item->id}}" class="btn btn-primary btn-sm">Read More</a>
                </form>
            </div>
          </div>
        </div>    
    @empty
        <h5>Tidak ada Berita</h5>
    @endforelse

</div>

@endsection