@extends('layout.master')
@section('judul')
    Edit Berita {{$berita->title}}
@endsection

@section('content')
<form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
    @method('put')
    @csrf
    <div class="form-group">
        <label >Judul Berita</label>
        <input type="text" class="form-control" name="title" value={{$berita->title}} placeholder="Masukkan judul Berita">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Content Berita</label>
        <textarea name="content" class="form-control" cols="30" rows="10">{{$berita->content}}</textarea>
        @error('content')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Kategori</label>
       <select name="kategori_id" id="" class="form-control">
           <option value="">--Silahkan Pilih Kategori--</option>
           @foreach ($kategori as $item)
           @if ($item->id === $berita->kategori_id)
               <option value="{{$item->id}}" selected>{{$item->name}}</option>
           @else
               <option value="{{$item->id}}">{{$item->name}}</option>
           @endif
           @endforeach
       </select>
        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label >Thumbnail Berita</label>
        <input type="file" class="form-control" name="thumbnail">
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection