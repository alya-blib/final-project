<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'IndexController@home');
Route::get('/form', 'FormController@biodata');
Route::post('/kirim', 'FormController@kirim');

Route::get('/data-table', function () {
    return view('table.data-table');
});

Route::get('/table', function () {
    return view('table.table');
});

Route::group(['middleware' => ['auth']], function () {
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

Route::resource('profile', 'ProfileController')->only([
    'index', 'update', 'edit']);

Route::resource('komentar', 'KomentarController')->only([
    'store']);

});



//Crud Kategori


//Crud Berita
Route::resource('berita', 'BeritaController');
Auth::routes();

