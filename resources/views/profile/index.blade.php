@extends('layout.master')


@section('content')
<div class="card card-primary card-outline">
    <div class="card-body box-profile">

      <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

      <p class="text-muted text-center">{{ Auth::user()->email }}</p>

      <ul class="list-group list-group-unbordered mb-3">
        <li class="list-group-item">
          <b>Umur</b> <a class="float-right">({{Auth::user()->profile->umur}} tahun)</a>
        </li>
        <li class="list-group-item">
          <b>Bio</b> <a class="float-right">{{Auth::user()->profile->bio}}</a>
        </li>
        <li class="list-group-item">
          <b>Alamat</b> <a class="float-right">{{Auth::user()->profile->alamat}}</a>
        </li>
      </ul>

      <a href="/profile/{{$profile->id}}/edit" class="btn btn-warning ml-auto">Edit</a>
      
    </div>
    <!-- /.card-body -->
  </div>
@endsection
