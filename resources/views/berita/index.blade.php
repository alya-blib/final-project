@extends('layout.master')
@section('judul')
    Tampil Berita
@endsection

@section('content')

@auth
<a href="/berita/create" class="btn btn-primary btn-sm my-2">Tambah</a>
@endauth

<div class="row">
    @forelse ($berita as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" >
            <div class="card-body">
              <h5>{{$item->title}}</h5>
              <span class="badge badge-info">{{$item->kategori->name}}</span>
              <p class="card-text">{{Str::limit($item->content, 50)}}</p>
             @auth
             <form action="/berita/{{$item->id}}" method="POST">
                @method('DELETE')
                @csrf
                <a href="/berita/{{$item->id}}" class="btn btn-primary btn-sm">Read More</a>
                <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" class="btn btn-danger btn-sm" value="DELETE">
                </form>
             @endauth
              
             @guest
             <a href="/berita/{{$item->id}}" class="btn btn-primary btn-sm">Read More</a>
             @endguest
            </div>
          </div>
        </div>    
    @empty
        <h5>Tidak ada Berita</h5>
    @endforelse

</div>


@endsection