@extends('layout.master')

@section('judul')
    Tambah Berita
@endsection

@section('content')
<form action="/berita" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label >Judul Berita</label>
        <input type="text" class="form-control" name="title" placeholder="Masukkan judul Berita">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Content Berita</label>
        <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
        @error('content')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Kategori</label>
       <select name="kategori_id" id="" class="form-control">
           <option value="">--Silahkan Pilih Kategori--</option>
           @foreach ($kategori as $item)
               <option value="{{$item->id}}">{{$item->name}}</option>
           @endforeach
       </select>
        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label >Thumbnail Berita</label>
        <input type="file" class="form-control" name="thumbnail">
        @error('thumbnail')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>



@endsection

