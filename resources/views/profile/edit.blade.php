@extends('layout.master')
@section('judul')
    Edit Profile
@endsection

@section('content')
<form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label >Nama User</label>
        <input type="text" class="form-control" name="umur" value="{{$profile->user->name}}" disabled>
    </div><div class="form-group">
        <label >Email</label>
        <input type="email" class="form-control" name="email" value="{{$profile->user->email}}" disabled>
    </div>
    <div class="form-group">
        <label >Umur</label>
        <input type="number" class="form-control" name="umur" value="{{$profile->umur}}" placeholder="Masukkan Umur Anda">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Biodata</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profile->alamat}}</textarea>
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection