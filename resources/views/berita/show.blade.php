@extends('layout.master')
@section('judul')
    Detail Berita {{$berita->judul}}
@endsection

@section('content')


<div class="row">
   
    <div class="col-12">
            <img src="{{asset('gambar/'.$berita->thumbnail)}}" width="200px" >
            <div class="card-body">
              <h5>{{$berita->title}}</h5>
              <p class="card-text">{!! $berita->content !!}</p>
          </div>
        </div>    
</div>
<div class="row my-3">
  <h2>Komentar</h2>
  @forelse ($berita->komentar as $item)
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <h5>{{$item->user->name}}</h5>
        <small>{{$item->created_at}}</small>
        <p>{!! $item->content !!}</p>
      </div>
    </div>
  </div>
  @empty
  <div class="col-12">
    <h1>Tidak ada Komentar</h1>
  </div>
      
  @endforelse
  
</div>
@auth
<div class="row">
  <div class="col-9">
    <form action="/komentar" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="form-group">
        <input type="hidden" value="{{$berita->id}}" name="berita_id">
          <label for="body">Komentar</label>
          <textarea name="content" class="form-control" placeholder="Masukan Komentar"></textarea>
          @error('content')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
      <button type="submit" class="btn btn-primary">Tambah Komentar</button>
  </form>
    
  </div>
</div>
@endauth

<a href="/berita" class="btn btn-secondary btn-sm my-2">Kembali</a>

@push('script')
<script src="https://cdn.tiny.cloud/1/ik3bguh2o6a5xat5tiwiox3bq0ocpxkp9y8o2laayhp8ufpa/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@endsection