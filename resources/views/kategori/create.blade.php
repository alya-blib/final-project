@extends('layout.master')
@section('judul')
    Tambah Kategori
@endsection

@section('content')
<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
        <label >Nama Kategori</label>
        <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Kategori">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Deskripsi Kategori</label>
        <textarea name="description" class="form-control" cols="30" rows="10"></textarea>
        @error('description')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/ik3bguh2o6a5xat5tiwiox3bq0ocpxkp9y8o2laayhp8ufpa/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush