<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $fillable = ['name', 'description'];

    public function berita()
    {
        return $this->hasMany('App\Berita');
    }

    
}
