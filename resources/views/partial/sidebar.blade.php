<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <span class="brand-text font-weight-light ml-3" >Berita Online Menit.com</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @auth
          <a href="/profile" class="d-block">{{ Auth::user()->name }} <br> ({{Auth::user()->profile->umur}} tahun)</a>
          @endauth

          @guest
              Belum Login
          @endguest
          
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Home
              </p>
            </a>
          </li>
          @auth
          <li class="nav-item">
            <a href="/kategori" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Kategori
              </p>
            </a>
          </li>
          @endauth
          

          <li class="nav-item">
            <a href="/berita" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Berita
              </p>
            </a>
          </li>

          {{-- @auth
              
          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>

          @endauth --}}
          @auth
          <li class="nav-item bg-danger">     
            <a class="nav-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
             {{ __('Logout') }}
            </a>
  
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
             @csrf
            </form>
            </li>
  
          @endauth 
          
          {{-- @guest
              
          <li class="nav-item bg-primary">
            <a href="/login" class="nav-link">
              <p>
                Login
              </p>
            </a>
          </li>
          @endguest --}}

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>