@extends('layout.master')
@section('judul')
    Tampil Kategori
@endsection

@section('content')
    
<a href="/kategori/create" class="btn btn-primary mb-2">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Body</th>
        <th scope="col" >Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->name}}</td>
                <td>{!! $value->description !!}</td>
                <td>
                    <a href="/kategori/{{$value->id}}" class="btn btn-primary">Show</a>
                    <a href="/kategori/{{$value->id}}/edit" class="btn btn-warning">Edit</a>
                    <form action="/kategori/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection