<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Berita;
use File;
use App\Exports\BeritaExport;
use Maatwebsite\Excel\Facades\Excel;
use RealRashid\SweetAlert\Facades\Alert;


class BeritaController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::get();
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::get();
        return view('berita.tambah', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('gambar'), $imageName);

        $berita = new Berita;

        $berita->title = $request->title;
        $berita->content = $request->content;
        $berita->kategori_id = $request->kategori_id;
        $berita->thumbnail = $imageName;

        $berita->save();

        Alert::success('Berhasil', 'Berhasil Menambahkan Berita!');

        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::find($id);
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::get();
        $berita = Berita::find($id);
        return view('berita.edit', compact('berita', 'kategori'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'kategori_id' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $berita = Berita::findorfail($id);

        if ($request->has('thumbnail')) {
            $path = "gambar/";
            File::delete($path . $berita->thumbnail);
            $imageName = time().'.'.$request->thumbnail->extension();
            $request->thumbnail->move(public_path('gambar'), $imageName);
            $berita_data = [
                'title' => $request->title,
                'kategori_id' => $request->kategori_id,
                'content' => $request->content,
                'thumbnail' => $imageName
            ];
        }else {
            $berita_data = [
                'title' => $request->title,
                'kategori_id' => $request->kategori_id,
                'content' => $request->content,
            ];
        }

        $berita->update($berita_data);

        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::findorfail($id);
        $berita->delete();

        $path = "gambar/";
        File::delete($path . $berita->thumbnail);

        return redirect('/berita');
    }

    public function export() 
    {
        return Excel::download(new BeritaExport, 'berita.xlsx');
    }
}
