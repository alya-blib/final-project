<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function biodata()
    {
        return view('halaman.biodata');
    }

    public function kirim(Request $request)
    {
        $nama = $request['nama'];
        $bio = $request['bio'];

        return view('halaman.index', compact('nama', 'bio'));
    }

}
